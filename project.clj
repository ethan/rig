(defproject rig "0.1.0-SNAPSHOT"
  :description "A modular, channel-based approach to working with Overtone."
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  ;; :plugins [[cider/cider-nrepl "0.9.1"]]
  :dependencies [
                 [org.clojure/clojure "1.5.1"]
                 [overtone "0.9.1"]
                 [clj-time "0.11.0"]
                 ;; [org.clojure/clojure "1.7.0"]
                 ;; [overtone "0.10-SNAPSHOT"]
                 [overtone/midi-clj "0.5.0"]
                 [org.clojure/clojurescript "1.7.122"]
                 [org.clojure/core.async "0.1.346.0-17112a-alpha"]
                 [org.clojure/data.json "0.2.6"]
                 [compojure "1.4.0"]
                 [http-kit "2.1.18"]
                 [reagent "0.5.1-rc"]
                 [re-frame "0.4.1"]
                 [ring "1.4.0"]
                 [ring/ring-json "0.4.0"]
                 ;; [monome-osc "0.1.0-SNAPSHOT"]
                 ]

  :plugins [[lein-cljsbuild "1.1.0"]]
            

  :cljsbuild {:builds [{:id "dev"
                        :source-paths ["src/frontend"]
                        :compiler {:output-to "resources/public/demo.js"
                                   :output-dir "resources/public/out"
                                   :optimizations :none
                                   :source-map true}}]}

  )
