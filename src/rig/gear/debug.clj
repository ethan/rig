(ns rig.gear.debug
  "Basic rig node to output all messages received"
  (:require
   [rig.node :as rn]
   [rig.message :as rm]
   [clojure.core.async
    :as async
    :refer [>! <! >!! <!! go chan close! thread
            alts! alts!! timeout mult]]
   ;; :reload
   )
  (:import
   [rig.node Node]
   )
  )

(alter-var-root #'*out* (constantly *out*))

;; TODO debug should really just use the generic node record.
(defn make-debug-node
  "Instantiate a debug node"
  [node-id]
  (let [in-chan (chan)
        out-chan (chan)
        out-mult (mult out-chan)
        in-handler (fn [node message]
                        (println message)
                        )
        ;; This could also be implemented via a defrecord, but I want to explore reify.
        new-node (rn/make-node node-id in-handler)
        ;; Note: an experiment with reify didn't work when requiring this in other ns's.
        ;; new-node (reify rn/RigNode
        ;; ...
        ;;            )
        ]
    new-node
    )
  )


(comment
  (def debugger (make-debug-node :midi-test-debugger))
  (rn/get-node-id debugger)
  (rn/rm-node debugger)
  )
