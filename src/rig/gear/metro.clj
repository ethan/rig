;; This is a basic wrapper around the overtone metronome object.
;;
;; Another option would be to use an SC control signal and
;; on-event. This wouldn't allow simple groove mappings but would
;; allow variation of the groove by modulating the clock signal, which
;; is also kind of cool.
;;
;; Note we redefine beats to be 'ticks' (fractions of a beat) and use
;; ticks per beat to extract whole beat values to be some fraction of a
;; beat (why overtone == beats and ticks I don't get.)"
;;
;; TODO refactor metro.clj to not use the clojure metro object, it's not worth it.
;; TODO make an alternate metro that's LFO and OSC signal based (SC clock)
(ns rig.gear.metro
  (:use [overtone.core])
  (:require [rig.node :as rn]
            [rig.message :as rm]
            ;; :reload
            )
  (:import [rig.node Node])
  )

;; ## Basic in handler to dispatch to metronome.
;; TODO output status changes as outs.
(defn metro-in-handler
  [node message]
  (case (:event message)
    :metro-start (metro-start (:metro node))
    :metro-start-at (let [start-beat (get-in message :data :start-beat)] (metro-start (:metro node) start-beat))
    ;; TODO implement metro-stop (using bpm=0 and a "stopped" flag?)
    ;; :metro-stop (metro-stop (:metro node))
    :metro-set-bpm (let [tpb (:tpb node)
                         bpm (* tpb (get-in message :data :bpm))]
                     (metro-bpm (:metro node) bpm))
    )
  )

(defn metro-node-tick [node]
  (dec (metro-beat (:metro node)))
  )

(defn metro-node-beat [node]
  (/ (metro-node-tick node) (:tpb node))
  )

(defn metro-node-beat-tick [node]
  (mod (metro-node-tick node) (:tpb node))
  )

(defn metro-node-bar-tick [node]
  (let [tick (metro-node-tick node)
        bar-ticks (metro-bpb (:metro node))
        ]
    (mod tick bar-ticks)
    )
  )

(defn metro-message-base-data [node]
  {:tick (metro-node-tick node)
   :beat-tick (metro-node-beat-tick node)
   :bar-tick (metro-node-bar-tick node)
   :beat (metro-beat (:metro node))
   :bar (metro-bar (:metro node))
   }
  )

(defn metro-out-tick
  "Output message on tick."
  [node]
  (let [message (rm/make-base-message node :metro-tick (metro-message-base-data node))
        ]
    (rn/out-put! node message))
  )

(defn metro-tick-loop
  [node]
  (let [metro (:metro node)
        tick (metro-node-tick node)
        next-time (metro-beat metro (inc tick))
        ]
    (metro-out-tick node)
    (if (not= true (:stopped? node)) (apply-at next-time #'metro-tick-loop [node]))
    ))


;; TODO set up bar and beat clock signals (maybe).

;; Experimenting with implementing as a basic hash with a node
;; property. I think it should just be a bit slower to look up the
;; non-specified props.
(defn make-metro
  "Instantiate a metro node for a rig.
  
  Takes node-id (keyword), bpm and ticks per beat as args."
  [node-id bpm tpb]
  (let [metro (metronome (* bpm tpb))
        metro-node (assoc (rn/make-node node-id metro-in-handler)
                          :metro
                          metro
                          :tpb
                          tpb
                          )
        ]
    (metro-bpb metro (* 4 tpb))
    ;; Set up loops on tick, beat and bar
    (apply-at (metro-beat (:metro metro-node) 0) #'metro-tick-loop [metro-node])
    metro-node
    )
  )

(comment
  (def test-metro-b (make-metro :metro-test 40 4))
  (metro-tick-loop test-metro-b)
  (metro-out-tick test-metro-b)
  (metro-beat (:metro test-metro-b) 1443.25)
  )
