;; # Rig Messaging Functions
;;
;; Messages are passed between nodes in the rig via core.async
;; channels. This namespace defines the most basic protocols and data
;; events for rig messages.
(ns rig.message
  (:require [clojure.core.async
             :as async
             :refer [>! <! >!! <!! go chan buffer close! thread
                     alts! alts!! timeout mult]]
            [clj-time.core :as time]
            [clj-time.coerce :as timec]
            [rig.node :as rn]
            )
  )

;; ## RigGraphMessage
;;
;; Basic message format for sending messages via channels between
;; nodes in the rig.
(defprotocol RigMessage
  ;; Return the node that is the origin of this message
  (get-origin [message])
  (get-timestamp [message])
  (get-event [message])
  (get-data [message])
  (get-origin-id [message])
  )

;; Used to describe messages output from bases in the rig.
(defrecord BaseMessage
    [origin timestamp event data]
    RigMessage
    (get-origin [message] (:origin message))
    (get-timestamp [message] (:timestamp message))
    (get-event [message] (:event message))     
    (get-data [message] (:data message))
    (get-origin-id [message] (:node-id (get-origin message)))
  )

(defn make-base-message
  "Contructor for creating new base messages"
  [origin event data]
  (let [timestamp (timec/to-long (time/now))]
    (->BaseMessage origin timestamp event data)
    )
  )
