;; # Controllers (and controllers) for an Overtone rig.
;;
;; The controller abstraction is used to implement logic for
;; incorporating physical interfaces (MIDI keyboards, Monomes, etc.),
;; GUIs, sequencers, etc. into a Rig.
;;
;; Controllers have input and output channels. They output events
;; (actions and state changes) or state dumps, and they take as input
;; imperatives (used to chnage their state, such as to turn on or off
;; LEDs).
;;
;; Controllers should never directly update their own internal state,
;; but should always use a loop with an external transducer to
;; transform their own event output into the desired state change
;; input. This is important so that other controllers can record the
;; output of any controller and play it back at a later time
;; (e.g. sequencing a series of inputs from a Monome).
;;
;; Controller event messages are implemented as defrecords that extend
;; the base controller-message type. They should always include a dump
;; of the full state of the controller after the event, as well as a
;; timestamp and an ID of the controller from which the message
;; originated.

(ns rig.controller
  (:use [rig.message])
  (:require [clojure.core.async
             :as async
             :refer [>! <! >!! <!! go chan buffer close! thread
                     alts! alts!! timeout]]
            [clj-time.core :as time]
            [clj-time.coerce :as timec]
            )
  )

(defrecord ControllerMessage
    "Used to describe messages output from controllers in the rig."
    [controller timestamp event data]
    RigMessage
    (get-source [msg] controller)
    (get-timestamp [msg] timestamp)
    ;; TODO need to finalize this message type format (proposing a tuple of type key and data)
    (get-type [msg] [:event event])     
    (get-data [msg] data)
  )

(defn make-controller-message
  "Contructor for creating new controller messages"
  [controller event data]
  (let [timestamp (timec/to-long (time/now))]
    (->ControllerMessage controller timestamp event data)
    )
  )

;; 
;; TODO: add handle-input method
(defprotocol Controller
  (get-in-chan [controller] "Accessor for input channel of controller")
  (get-out-chan [controller] "Accessor for output channel of controller")
  (get-out-mult [controller] "Accessfor for output channel's mult")
  (tap-out-mult [controller] "tap the out mult of the controller")

  ;; TODO is this really just a generalizable RigNode?
  ;; RigNodes would tag inputs and transduce them to actions according
  ;; to a "map-in" function or similar. And would route events from
  ;; other systems to messages on their outputs.
  ;; 
  ;; We'd most likely use reify to pass a transducer function to our
  ;; node a spart of the custom constructor function.
  (map-in [message])
  )

(defrecord GenericController [in out out-mult]
    Controller
    ;; TODO implement a demonstration controller here.
    )

