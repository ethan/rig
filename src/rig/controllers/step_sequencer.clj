(ns rig.controllers.step-sequencer
  (:require [rig.node :as rn]
            [rig.message :as rm]
            )
  ;; TODO does this also need an import statement for rn.RigNode?
  (:import (rn.Node))
  )

;; The step-sequencer is a node that takes a clock input and outputs
;; triggers with keywords.
;;

;; Set a single step, either single track or all tracks.
(defn set-step
  ([sequencer step data]
   (let [step-ref (nth (:steps sequencer) step nil)]
     (dosync
      (if step-ref
        (ref-set (nth (:steps sequencer) step) data)))))
  ([sequencer step track data]
   (let [step-ref (nth (:steps sequencer) step nil)]
     (if (and step-ref (seq data))
       (dosync
        (alter step-ref
               assoc
               track
               data
               )
        )
       (dosync
        (alter step-ref
               dissoc
               track
               ))
        )
     ))
  )

(defn clear-step
  [sequencer step]
  (set-step sequencer step {})
  )

;; Set all steps for a single track.
(defn set-track
  [sequencer track steps-data]
  (dotimes [step (count (:steps sequencer))]
    (set-step sequencer step track (nth steps-data step {}))
    )
  )

;; Get the value for all tracks on a single step.
(defn get-step
  ([sequencer track step]
   (let [step-ref (nth (:steps sequencer) step nil)]
     (if step-ref
       (get-in (deref step-ref) track))))
  ([sequencer step]
   (let [step-ref (nth (:steps sequencer) step nil)]
     (if step-ref
       (deref step-ref))))
  )

(defmulti step-sequencer-in-handler
  "Handle input messages sent to the sequencer.
  Understands the following events: 

  * set-track-step: set the value for a single track, a single step
  * set-track-steps: set all steps for a single track, as vector 
  * set-all-steps: set all steps for all tracks, hashmap of vectors
  * clear-all-steps: unset all setps
  * dump: dump hashmap of vecotrs (same as input for set-all)
  * play-step: trigger a single tick (see rig.gear.clock for tick format.)

  "
  (fn [node message] (:event message)))

(defmethod step-sequencer-in-handler :set-track-step
  [node message]
  (let [step (-> message :data :step)
        track (-> message :data :track)
        step-data (-> message :data :step-data)
        ]
    (set-step node step track step-data)
    )
  )

(defmethod step-sequencer-in-handler :set-track-steps
  [node message]
  (let [track (-> message :data :track)
        track-data (-> message :data :steps-data)]
    (set-track node track track-data)
    )
  )

(defmethod step-sequencer-in-handler :set-all-steps
  [node message]
  ;; TODO implement tracks bulk set
  )

(defmethod step-sequencer-in-handler :dump
  [node message]
  ;; TODO implement tracks dump
  )

(defmethod step-sequencer-in-handler :clear-all-steps
  [node message]
  (dotimes [n (count (:steps node))]
    (clear-step node n)
    )
  )

(defmethod step-sequencer-in-handler :play-step
  [node message]
  (let [step (-> message :data :step)
        step-data (get-step node step)
        step-message (rm/make-base-message node
                                          :sequencer-step
                                          step-data)
        ]
    (if (seq step-data) (rn/out-put! node step-message))
    )
  )

(defn make-step-sequencer
  "Factory to create step sequencer nodes

  Properties: # tracks (as a vector of keywords), steps,
  step-length (in ticks).
  "
  [& {:keys [node-id tracks step-count]}]

  (let [
        new-node (rn/map->node :node-id node-id
                               :in-handler step-sequencer-in-handler
                               ;; Steps are recorded as a vector of hashmap refs, so we can easily get each one.
                               ;; TODO should this be an agent (e.g. async)
                               :properties {:steps (map ref (repeat step-count {}))
                                            :tracks tracks
                                            }
                               )
        ]
    new-node
    )
  )
