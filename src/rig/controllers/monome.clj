;; An Overtone Rig controller for working with the Monome.
(ns rig.controllers.monome
  (:use
   [rig.controller]
   )
  (:require
   [clojure.core.async
    :as async
    :refer [>! <! >!! <!! go chan buffer close! thread
            alts! alts!! timeout]]
   [monome-osc.core :as monome]
   )
  )

;; ## MonomeController Class
;;
;; This is the primary development use case for this library.
;;
;; The Monome can send button on/off events as well as tilt events, it
;; receives LED on/off messages, either individual or via various
;; batch operation options.
(defrecord MonomeController [monome in out out-mult]
  Controller
  (get-in-chan [controller] in)
  (get-out-chan [controller] out)
  (get-out-mult [controller] out-mult)
  (tap-out-mult [controller] (async/tap mult))
  (handle-in [message]
    ;; TODO implement pipe/transducer/handler for dispatching valit inputs to the monome.
    )
  )

(def monome-in-transducer
  "Pre-process input (filter out invalid messages, etc).
  THIS MAY NOT BE NEEDED WITH MULTIMETHODS FOR TAKE-IN func.
  "
  []
  )

(defmulti monome-in-handler
  "Perform side-effect-ful operations based on input messages, in this
  case invoking methods from the monome-osc library.

  Examples might be taking this RigMessage:

  `{
    :source monome-router
    :type :action
    :op  :set-led
    :data {
      :pos [4 5]
      :state 1
    }
  }`

  And executing this code

  `(monome/set-led 4 5 1)`

  NOTE: the format of the ':type' key is NOT YET DEFINED OR FINAL."
  (fn [controller message] [(:type message) (:op message)])
  )

(defmethod monome-in-handler [:action :set-led]
  (let [monome (:monome controller)]
    (apply monome/set-led (flatten [monome (:pos (:data message))]))
    )
  )

;; NEXT TODO: incorporate monome-in-handler to be run as part of a
;; take! loop, either in MonomeController, or as part of another
;; protocol/record type that MonomeController is a reification of
;; (e.g. make-monome-controller just delegates to make-rig-node and
;; passes the "in-handler" arg).

;; Constructor for setting up a monome controller given a specific
;; monome osc object.
(defn make-monome-controller [monome]
  ;; TODO determine if this works or if we need to make these chans in
  ;; a separate scope (e.g. (->MonomeController (chan) (chan) monome))
  (let [in (chan)
        monome-listener (monome/listen-to monome)
        monome-mult (mult monome-listener)
        ;; TODO add transducer to transform this to proper controller
        ;; output as per spec.
        monome-tap (async/tap monome-mult (async/chan))
        ; TODO: add monome state change output channel. add to merge below.
        out (merge monome-tap)
        out-mult (mult out)
        new-monome-controller (->MonomeController monome in out out-mult)
        ]
    ;; Set up listener on monome 
    new-monome-controller
    )
  )

(comment
  ;; RigNode Variant
  ;; step 1: define MonomeController as implementing RigNode, not
  ;; controller, or as mixing both in...
  (defn make-monome-controller [monome]
    (let [in (chan)
          monome-listener (monome/listen-to monome)
          monome-mult (mult monome-listener)
          ;; TODO add transducer to transform this to proper controller
          ;; output as per spec.
          monome-tap (async/tap monome-mult (async/chan))
          ;; out (merge monome-tap)
          new-monome-controller (make-rig-node monome-in-handler)
          ]
      ;; Set up listener on monome 
      new-monome-controller
      )
    )
  )

