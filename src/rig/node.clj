(ns rig.node
  (:require
   [clojure.core.async
    :as async
    :refer [>! <! >!! <!! go chan buffer close! thread
            alts! alts!! timeout mult]]
   )
  )

;; Generate the basic code for putting a message on a channel.
;; TODO should this return the whole function definition for defrecords implementing node?
(defmacro default-in-put! [node message]
  `(let [in-chan# (:in ~node)]
     (go
       (>! in-chan# ~message))
     )
  )

;; Generate the basic code for putting a message on a channel.
;; TODO should this return the whole function definition for defrecords implementing node?
(defmacro default-out-put! [node message]
  `(let [out-chan# (:out ~node)]
     (go
       (>! out-chan# ~message))
     )
  )

;; Set up the loop to read the input chan and pass to our handler.
;; 
;; We extract the code to init the input loop so we can easily use it
;; in the constructor functions for other types that implement or
;; extend NodeProtocol.
(defmacro default-in-handler-loop
  "Set up go-loop to listen on in channel and execute handler."
  [node]
  `(let [loop-chan# (async/go-loop []
           (when-let [
                      message# (async/<! (:in ~node))
                      ]
             ;; TODO Is there a better way to do this so the in-handler
             ;; can be changed without needing to create new channels?
             (apply (deref (:in-handler ~node)) [~node message#])
             (recur)
             )
           )
         ]
     
     (dosync (ref-set (:in-loop ~node) loop-chan#))
     )
  )

;; A node is basically just an input and output channel, a mult for
;; the output and a function to loop on the input channel.
;;
;; TODO: I'd like to call this NodeProtocol, is that okay?
(defprotocol RigNode
  ;; (get-node-id [node])
  ;; (get-in-chan [node])
  ;; (get-out-chan [node])
  ;; (get-out-mult [node])
  ;; (get-in-handler [node])
  (init-in-loop [node])
  (kill-in-loop! [node])
  (tap-out-mult [node chan])
  (out-put! [node msg])
  (in-put! [node msg])
  (close-chans! [node])
  (rm-node [node])
  )

;; Quick test to see if a node is a rig node.
(defn is-rig-node?
  "Boolean check if object is a rig node."
  [obj]
  (or
   (extends? RigNode (type obj))
   )
  )

;; Implement the basic RigNode at its most basic.
(defrecord Node [node-id in out out-mult in-handler in-loop]
  RigNode
  ;; TODO abstract all standard node creation code into a macro.
  ;; Q: How to make a macro that generates multiple forms?
  ;; TODO these should be deprecated as per clojure standards, use :key
  ;; (get-node-id [node] (:node-id node))
  ;; (get-in-chan [node] (:in node))
  ;; (get-out-chan [node] (:out node))
  ;; (get-out-mult [node] (:out-mult node))
  ;; (get-in-handler [node] (:in-handler node))
  (init-in-loop [node]
    (default-in-handler-loop node)
    )
  (kill-in-loop! [node]
    ;; Close the go loop returned by the in handler setup func to terminate the loop.
    (async/close! (deref (:in-loop node)))
    )
  (tap-out-mult [node chan]
    (async/tap (:out-mult node) chan)
    )
  (in-put! [node message]
    (default-in-put! node message)
    )
  (out-put! [node message]
    (default-out-put! node message)
    )
  (close-chans! [node]
    (async/untap-all (:out-mult node))
    (async/close! (:in node))
    (async/close! (:out node))
    )
  (rm-node [node]
    (kill-in-loop! node)
    (close-chans! node)
    )
  )


;; TODO: should this be multi-arity to accept out own out channels,
;; and such? (e.g. in the case of monome, where output is a transduced
;; chan from the monome lib.)
(defn make-node
  [id in-handler & rest]
  "Construct a new node object given an ID key and a function to
  handle incoming message signals."
  (let [in (chan)
        out (chan (* 1024 512)) ; Set some buffering out output chans so we don't go too crazy.
        out-mult (mult out)
        in-handler (ref in-handler)  
        in-loop (ref nil)
        new-node (->Node id in out out-mult in-handler in-loop)
        ;; It doesn't work assoc the properties on after the node is
        ;; created because the closure in the in-handler passes the
        ;; node that doesn't have the variables assoc'd, so there is
        ;; no :synth-key yet (it gets added on a later iteration of
        ;; the object, both exist in parallel)
        ;;
        ;; So we assoc them before init'ing the loop.
        ;;
        ;; This is used like "(make-node :nid (fn...) :prop1 val :prop2 val etc...)"
        new-node (if rest (apply assoc new-node rest) new-node)
        ]
    (init-in-loop new-node)
    new-node
    )
  )

(defn map->node [& {:keys [node-id in-handler properties]}]
  (apply (partial make-node node-id in-handler) (reduce into properties))
  )

;; TODO implement agent to track all connections

(defn connect [connect-node-id from-node to-node in-handler]
  (let [
        connection (make-node connect-node-id in-handler)
        in-tap (tap-out-mult from-node (:in connection))
        out-tap (tap-out-mult connection (:in to-node))
        ]
    connection
    )
  )
