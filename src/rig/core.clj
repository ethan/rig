;; # Overtone Rig
;;
;; A rig is a collection of devices configured in a way that makes it
;; easy to make sound...and jam out.
;;
;; This library is intended to provide the basic for setting up sound
;; processing rigs and their associated controllers. The rig stack is
;; made up of the following components:
;;
;; 1. A standard SuperCollider group, channel and buffer organization
;;   for extensible connections, along with Overtone-based functions for
;;   manipulating and utilizing that configuration.
;; 2. A graph (as in network) of clojure objects controlling and
;;    interfacing with the SuperCollider sound generation and maniulation
;;    entities.
;; 3. A protocol for passing messages between any nodes in the graph,
;;    using `clojure.core.async`.
(ns rig.core)

;; TODO expose controllers, etc. using standard immigrate(?) stuff.

