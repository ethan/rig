;; # Rig Player
;; 
;; Controll Overtone/SC Synths via channel messages.
;;
;; This namespace holds objects and constructors for controlling
;; synths (generators, effects, etc.) in scsynth/overtone via
;; RigMessages passed over core.async channels.
;;
;; Player creator takes a function that transforms the structured map
;; of synth values to an ordered vector suitable for sending to
;; scsynth.
;;
;; ## Message Types
;; 
;; The player understands the following message types:
;;
;; ### synth-on
;; 
;; Turns on a single synth instance. This may be used for "note-on" in
;; polyphonic synths, etc., for starting effect instances, etc.
;;
;; `synth-on` messages should include an "id" data key, which is used
;; for tracking individual instances in polyphonic instruments, etc.,
;; as well as a vector of parameters to send to the synth (frequency, etc.)
;; 
;; ### synth-off
;;
;; turn off a specific instance (id) of the synth, usually by sending a gate arg of 0.
;;
;; ### set-ctl
;;
;; Set a specific argument value (using the `ctl` overtone fuction).
(ns rig.player
  (:use
   [overtone.live] ; TODO remove dep on overtone, use rig.rack only.
   )
  (:require
   [clojure.core.async
    :as async
    :refer [>! <! >!! <!! go chan close! thread
            alts! alts!! timeout]]
   [rig.node :as rn]
   [rig.message :refer :all]
   [rig.rack :refer :all]
   ;; :reload-all
  )
  )

;; ## Player Protocol
;;
;; Implement functionality specificallly related to tracking playing
;; of synths.
(defprotocol PlayerProtocol
  (get-target [player id] "Get the tracked instance of a synth with a given id.")
  (store-target [player id instance] "Add the tracked instance of a synth to the instances stack.")
  )

;; ## Player Record
;;
;; Player node in a rig graph, handles interfacing with synths.
;;
;; TODO: ideal is to delegate from in-handler to various helper funcs,
;; what is the best way to structure the record for that? One idea is
;; to include a "do-fns" map with helper fns. Another is to extend the
;; record with ad-hoc fns and suffer slight penalties, another is to
;; just bake them in to the in-handler in the constructor and not
;; worry too much about cases where we might modify an individual
;; handler later.
;;
;; For speed, I'll choose the last strategy for now.
(defrecord Player
  [node-id in out out-mult instances synth-key in-handler-fn args-xform-fn]
  PlayerProtocol
  (get-target [player id]
    (get (deref (:instances player)) id)
    )
  (store-target [player id instance]
    (send (:instances player) assoc id instance)
    )
  )

;; Extend Player so it implements basic RigNode protocol methods.
(extend-type Player
  rn/RigNode
  ;; TODO refactor node into simple functions enforced via schema and :pre.
  (get-node-id [player] (:node-id player))
  (get-in-chan [player] (:in player))
  (get-out-chan [player] (:out player))
  (get-out-mult [player] (:out-mult player))
  (tap-out-mult [player chan]
    (async/tap (rn/get-out-chan player) chan)
    )
  (in-chan-put! [player message]
    (rn/default-in-chan-put! player message)
    )
  )

(defn default-do-set-ctl-fn
  "Default 'note-off' function."
  [player id arg value]
  (let [target (get-target player id)
        ]
    (ctl target arg value)
    )
  )

;; Default function for triggering a synth.
(defn default-do-synth-on-fn
  "Default function for triggering a synth."
  [player id args]
  (let [synth-key (:synth-key player)
        synth-instance (apply (partial play synth-key) args)] ; TODO is it easier to just pass args as a vector arg?
    (store-target player id synth-instance)
    ;; TODO Send output chan notification of synth-on
    synth-instance
    )
  )

(defn default-do-synth-off-fn
  "Default 'note-off' function."
  [player id]
  (default-do-set-ctl-fn player id :gate 0)
  )

;; TODO Q: should this be a macro?
(defn make-player
  "Create a player node in a rig graph setup."
  [&
   {:keys [node-id args-xform-fn synth-key do-synth-on-fn do-synth-off-fn do-set-ctl-fn]}]
  {:pre [args-xform-fn
         node-id
         ;; In order to use the default functions, we need a
         ;; synth-key, else we need all custom funcs.
         (or synth-key (and do-synth-on-fn do-synth-off-fn do-set-ctl-fn))
         ]}
  (let [instances (agent {})
        do-synth-on-fn (or do-synth-on-fn default-do-synth-on-fn)
        do-synth-off-fn (or do-synth-off-fn default-do-synth-off-fn)
        do-set-ctl-fn (or do-set-ctl-fn default-do-set-ctl-fn instances)
        ;; TODO refactor as multi-method
        in-handler-fn (fn [player message]
                        (println message)
                        (case (get-event message)
                          :synth-on (let [args (args-xform-fn message)
                                           id (:id (get-data message))
                                           ]
                                       (apply do-synth-on-fn [player id args]))
                          ;; TODO I'd like to make this more DRY, but
                          ;; how to do so before we've created the
                          ;; player object? (can't use get-target method)
                          :synth-off (let [id (:id (get-data message))
                                           ]
                                        ;; TODO this should be refactored as a method on player,
                                        ;; taking player instead of instances.
                                        (apply do-synth-off-fn [player id]))
                          :set-ctl (let [id (:id (get-data message))
                                         ]
                                        (apply do-set-ctl-fn [player id message]))
                          )
                        )
        in (chan)
        out (chan)
        out-mult (async/mult chan)
        new-player (->Player node-id in out out-mult instances synth-key in-handler-fn args-xform-fn)
        ]
    ;; Set up the in loop
    (rn/node-in-init new-player)
    ;; Return the new player.
    new-player
    )
  )

(comment
  ;; An example player constructor invocation might look like this:

  (make-player
   :id :player-pad2-a

   ;; We specity the xform-fn function separate in any case, whether
   ;; using idiomatic rig setup or custom do-* funcs:
   :args-xform-fn (fn
                 "Transform message map into synth arg vector to pass
                 to fm-synth via apply."
                 [message]
                 (let [{:keys [midi-note
                               amp
                               index
                               fm-amount]} (:data message)
                        freq (midi-cps midi-note)
                        gate 1]
                   ;; TODO test if "nil" for amp causes a 0 amp value
                   ;; or uses the definst default.
                   [freq amp index fm-amout gate]
                   )
                 )

   ;; Might just take synth key, assuming a standard Rig Overtone setup:
   :synth-key :fm-synth

   ;; OR it can take custom on, of and set-ctl functions as args:
   :do-synth-on-fn (fn
              "Actually play the synth. The returned synth object is
              stored in an agent inside the player using the id as a
              key."
              [args]
              (apply (partial rig/play :fm-synth) args)
              )
   :do-synth-off-fn (fn
                      "Turn off the synth instance associated with a
                      prior synth on"
                      [target]
                      (ctl target :gate 0)
                      )
   :do-set-ctl-fn (fn
                    "Set value of a synth control value"
                    [target key value]
                    (ctl target key value)
                    )
   )
  )
