(ns midisynth.tapeloop
  (:use [overtone.core])
  (:require [midisynth.rig :as rig])
  )

;; We need a buffer, two control busses, an input bus and an output bus, and the instruments.

;; Let's start by making without rig dependency, then we can hook up to rig.

(defn init
  "Initialize global settings for all loopers"
  [& {:keys []}]
  )
(odoc in-feedback)

(defn new
  "Create a new tape loop configuration.
  Arguments are:
  :annotation
  :in-bus inputs
  :out-bus outputs
  :feedback-bus feedback chan
  :loop-buffer buffer to use as loop tape
  :length length of max loop in samples (defaultsto 10sec at 44.1khz)
  :channels (defaults to 2)
  :record-head-group
  :play-head-group
  "
  [& {:keys [annotation in-bus out-bus feedback-bus length channels record-head-group play-head-group loop-buffer]
      :or [
           channels 2
           in-bus (audio-bus channels)
           out-bus (audio-bus channels)
           feedback-bus (audio-bus channels)
           length (* 44100 10)
           record-head-group (group (str annotation " record"))
           play-head-group (group (str annotation " play"))
           ]}]
  (let [
        write-idx-bus (control-bus)
        loop-buffer (if (buffer? loop-buffer) loop-buffer (buffer length channels))
        ]
    {:in-bus in-bus
     :out-bus out-bus
     :feedback-bus feedback-bus
     :write-idx-bus write-idx-bus
     :length length
     :record-head-group record-head-group
     :play-head-group play-head-group
     :loop-buffer loop-buffer
     }
    )
  )

(definst record-head
  "Record head of a tape loop, with ctl of position and feedback."
  [in-bus 10 feedback-bus 11 tape-buffer 0 write-idx-bus 1 channels 2]
  (let [in-sig (in in-bus channels)
        fb-sig (in-feedback feedback-bus channels)
        write-idx (in write-idx-bus) ; TODO should this be samples or 0-1?
        write-sig (+ in-sig fb-sig)
        ]
    (buf-wr write-sig tape-buffer write-idx 1)
    )
  )

(definst play-head
  "Play head of a tape loop"
  [out-bus 0
   feedback-bus 100
   tape-buffer 0
   read-idx-bus 1
   out-level 1
   feedback-level 0
   channels 2
   ]
  (let [read-idx (in read-idx-bus)
        out-sig (buf-rd channels tape-buffer read-idx)
        feedback-sig (* out-sig feedback-level)
        ]
    (out feedback-sig feedbac-bus channels)
    (out (* out-sig out-level) out-bus channels)
    )
  )

;; TODO: need to make the index generting ctl insts
;; these will need to use buf-rate-scale and others, likely
;; http://doc.sccode.org/Classes/BufRateScale.html
