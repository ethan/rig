(ns midisynth.webctl
  (:use [overtone.live]
        )
  (:require
   [org.httpkit.server :as h]
   [ring.middleware.reload :as ring-reload]
   )
  )

(defn app
  "Lame app for quick hacking."
  [request]
  (h/with-channel request socket
    
    )
  {:status 200
   :headers {}
   :body "SUCESS"
   }
  )

(defonce server (atom nil))

(comment
  (start-server {:port 3000})
  )

(defn start-server [options]
  (reset! server (h/run-server (ring-reload/wrap-reload #'app) options)))

(defn stop-server []
  (when-not (nil? @server)
    (@server :timeout 100)
    (reset! server nil)))

(defn -main [& args]
  (start-server {:port 3000})
  (println "Ready."))

(comment
  (stop-server)
  )
