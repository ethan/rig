(ns midisynth.sequence
  (:use [overtone.core]
        )
  )

(defmacro apply-by-bar
  "Super-simple macro to apply by a bar ms value."
  [m bar-num & rest]
  (let [bar-ms (metro-bar m bar-num)
        ]
    `(apply-by ~bar-ms ~@(rest))
    )
  )

(defn metro-next-nth-periodic-bar
  "Calculate the ms of the next bar of a given period, skipping n periods."
  [m bar-period period-count]
  (let [bar-cur (metro-bar m)
        next-bar-period-num (+ bar-cur (- bar-period (mod bar-cur bar-period) ))
        bar-n-period-num (+ next-bar-period-num (* bar-period period-count))
        ]
  bar-n-period-num)
  )

(defn metro-next-periodic-bar
  "Calculate the ms of the next bar of a given period."
  [m bar-period]
  (metro-next-nth-periodic-bar m bar-period 0)
  )

;; TODO need to refactor this to put the entire let into the macro and use gensym form to escape
;; http://www.braveclojure.com/writing-macros/#6_1__Variable_Capture
(defmacro apply-by-next-n-bar
  "Schedule a function by n bars in the future"
  [m bar-period & rest]
  `(let [
         next-bar-num# (metro-next-periodic-bar ~m ~bar-period)
         ]
     (apply-by-bar ~m next-bar-num# ~@rest)
     )
  )

(defmacro lettest
  [a]
  (let [out (str "TEST" (eval a))]
    `(str ~out "TEST")
    )
  )

;; TODO functions for applying N times
;; options include:
;; 1. scheduling the same function to run by a given period, n times
;; 2. a "periodic" macro that builds in some tracking of n runs

