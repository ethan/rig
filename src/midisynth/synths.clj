(ns midisynth.synths
  (:use [overtone.live]
        [overtone.synth.sts :only [prophet]])
  ;; (:require
   ;; )
  )

(defsynth drone-fm [carrier 440 divisor 2.0 depth 1.0 amp 0.5 out-bus 0]
  (let [modulator (/ carrier divisor)
        ]
    (out out-bus (pan2 (* amp 
                          (sin-osc (+ carrier
                                      (* (* carrier depth) (sin-osc modulator)))))))))

(defsynth saw-mod-sync-3 [freq1 440 freq2 440 freq3 440 out-bus 0]
  "Doubly-syncd saw, using modulo to calculate sync."
  (let [
        ;; TODO: the below fails due to sc controlproxy not being a comparable
        ;; for the time being freq1 < freq2 < freq3
        ;; [sfreq1 sfreq2 sfreq3] (sort [freq1 freq2 freq3])
        ratio1 (/ freq2 freq1)
        ratio2 (/ freq3 freq1) ; TODO: is this right?
        ;; TODO: this will produce artifacts when sweeping syncs, can we do a real restart by phase?
        saw1 (mod (* (saw freq1) ratio1) 1)
        saw2 (mod (* saw1 ratio2) 1)
        ]
    ;; TODO: make this true stereo
    (out out-bus saw2))
  )
  
(defsynth sync-phasor-3 [freq1 440 freq2 440 freq3 440 out-bus 0]
  "Doubly-syncd saw, using modulo to calculate sync."
  (let [
        sr (sample-rate)
        phasor2-rate (/ freq2 sr)
        phasor3-rate (/ freq3 sr)
        osc1 (impulse:ar freq1) ; master master is impluse, force reset
        osc2 (phasor:ar osc1 phasor2-rate)
        ;; Because the phasor wraps on cycle (not resets) need to check for descent
        osc2-delayed (delay1 osc2)
        osc3 (phasor:ar (- osc2-delayed osc2) phasor3-rate) ; if last cycle is greater than this, trig
        ]
    ;; TODO: make this true stereo
    (out out-bus (- (* osc3 2) 1))
    )
  )

