(ns midisynth.just-monome
  (:use [overtone.live]
        )
  (:require [midisynth.just :as just]
            [midisynth.synths :as synth]
            [monome-osc.core :as monome]
            [clojure.core.async
             :refer :all
             :exclude [map reduce into partition partition-by take merge]
             :as async]
            )
  )

(comment
  
  )
