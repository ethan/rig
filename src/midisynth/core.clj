(ns midisynth.core
  (:use [overtone.live]
        [overtone.synth.sts :only [prophet]])
  (:require
   [overtone.midi :as midi]
   )
  )

(comment
  ;; Basic MIDI Example

  
  
  (on-event [:midi :note-on]
            (fn [m]
              (let [note (:note m)]
                (prophet :freq (midi->hz note)
                         :decay 5
                         :rq 0.6
                         :cutoff-freq 1000
                         :amp (:velocity-f m))))
            ::prophet-midi)

  (remove-event-handler ::prophet-midi))



(comment

  ;; Another midi synth
  
  
  (defsynth pad2 [freq 440 amp 0.4 amt 0.3 gate 1.0 out-bus 0]
    (let [vel        (+ 0.5 (* 0.5 amp))
          env        (env-gen (adsr 0.01 0.1 0.7 0.5) gate 1 0 1 FREE)
          f-env      (env-gen (perc 1 3))
          src        (saw [freq (* freq 1.01)])
          signal     (rlpf (* 0.3 src)
                           (+ (* 0.6 freq) (* f-env 2 freq)) 0.2)
          k          (/ (* 2 amt) (- 1 amt))
          distort    (/ (* (+ 1 k) signal) (+ 1 (* k (abs signal))))
          gate       (pulse (* 2 (+ 1 (sin-osc:kr 0.05))))
          compressor (compander distort gate 0.01 1 0.5 0.01 0.01)
          dampener   (+ 1 (* 0.5 (sin-osc:kr 0.5)))
          reverb     (free-verb compressor 0.5 0.5 dampener)
          echo       (comb-n reverb 0.4 0.3 0.5)]
      (out out-bus
           (* vel env echo))))

  (defonce memory (agent {}))

  (on-event [:midi :note-on]
            (fn [m]
              (send memory
                    (fn [mem]
                      (let [n (:note m)
                            s (pad2 :freq (midi->hz n))]
                        (assoc mem n s)))))
            ::play-note)

  (on-event [:midi :note-off]
            (fn [m]
              (send memory
                    (fn [mem]
                      (let [n (:note m)]
                        (when-let [s (get mem n)]
                          (ctl s :gate 0))
                        (dissoc mem n))))
              )
            ::release-note)

  (remove-event-handler ::play-note)
  (remove-event-handler ::release-note))

(comment
  ;; Working with abstractions

  
  

  (definst ding
    [note 60 velocity 100]
    (let [freq (midicps note)
          snd  (sin-osc freq)
          env  (env-gen (perc 0.1 0.8) :action FREE)]
      (* velocity env snd)))

  (defn midi-player [event]
    (ding (:note event) (/ (:velocity event) 127.0)))

  (def keyboard (midi-in))
  (midi-connected-devices)

  (def nk (midi-find-connected-device "Port 1"))
  (def nk (midi-find-connected-device "IAC Driver Bus 1"))

  (midi-mk-full-device-key nk)

  (definst poly-ding
    [note 60 amp 1 gate 1]
    (let [freq (midicps note)
          snd  (sin-osc freq)
          env  (env-gen (adsr 0.001 0.1 0.6 0.3) gate :action FREE)]
      (* amp env snd)))

  (midi/midi-handle-events nk #'midi-player))

