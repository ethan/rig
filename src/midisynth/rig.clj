;; TODO add control routing
;; TODO make mix controls k controled

(ns midisynth.rig
  (:use [overtone.live]
        )
  ;; (:require
  ;;  )
  )

;; Signal Routing

;; TODO: add panning (q: how to pan a stereo in sig?)
(defsynth stereo-chan-router
  "Basic router and mix channel for stereo buses. Level is in dB, not linear."
  [in-bus 10 out-bus 0 level-db 0 pan 0]
  (let [input (in in-bus 2)
        amp-mult (dbamp level-db)
        output (* amp-mult (balance2 (first input) (last input) pan))
        ]
    (out out-bus output)
    )
  )

(defsynth mono2stereo-chan-router
  "Basic router and mix channel for mono buses going into stereo mains"
  [in-bus 10 out-bus 0 level-db 0 pan 0]
  (let [input (in in-bus)
        amp-mult (dbamp level-db)
        output (* amp-mult (pan2 input pan))
        ]
    (out out-bus output)
    )
  )

(defsynth mono-chan-router
  "Basic router and mix channel for mono buses going into stereo mains"
  [in-bus 10 out-bus 0 level-db 0]
  (let [input (in in-bus)
        amp-mult (dbamp level-db)
        output (* amp-mult input)
        ]
    (out out-bus output)
    )
  )

;; Set up groups for controls, signals and effects
;; TODO structure these somehow

(defonce rig-g (group "rig root")) ; root
(defonce k-g (group "control group" :head rig-g)) ; control sigs
(defonce sig-g (group "signal group" :after k-g)) ; related signal stuff
(defonce pre-g (group "pre group" :head sig-g)) ; pre-gens (ins, etc)
(defonce gen-g (group "gen group" :after pre-g)) ; generators
(defonce post-g (group "post group" :after gen-g)) ; post-generator (processing, etc)
(defonce route-g (group "routing group" :after sig-g)) ; route sigs to fx
(defonce fx-g (group "effects group" :after route-g)) ; process fx
(defonce chan-g (group "chanels group" :after fx-g)) ; route chans to main, monitor, etc
(defonce out-g (group "main group" :after chan-g)) ; route mixes to outs


;; TODO: make rack and atom
(defonce rack {})

;; Set up default rack

(defmacro mix-for-outs [in-bus out-bus group outs]
  (if (= 1 outs)
    `(mono2stereo-chan-router [:tail ~group] ~in-bus ~out-bus)
    `(stereo-chan-router [:tail ~group] ~in-bus ~out-bus)
    )
  )

;; (macroexpand '(mix-for-outs 1 0 chan-g 1))

;; (defonce rack {
;;                :main (make-chan :main "Main Mix" :mix-bus 0 :group out-g)
;;                })

(defn rack-out [chan-key]
  (get-in rack [chan-key :out]))

(defn rack-in [chan-key]
  (get-in rack [chan-key :in]))

(defn rack-send [chan-key fx-key]
  (get-in rack [chan-key :sends fx-key]))

(defn rack-mix [chan-key]
  (get-in rack [chan-key :mix]))

(defn rack-synth [chan-key]
  (get-in rack [chan-key :synth]))

;; Main is different: the in is mixed directly to the out
(defn make-main-chan []
  (let [bus-in (audio-bus 2 (str "IN:MAIN"))
        mix (mix-for-outs bus-in 0 out-g 2)
        ]
    (def rack (assoc rack :main {:in bus-in
                                    :out 0
                                    :mix mix
                                    }))
    )
  )

(defn make-chan [chan-key name-str & {:keys [ins outs group mix-bus synth] :or {ins 2 outs 2 mix-bus (rack-in :main) group chan-g}}]
  (let [
        ;; mix-bus (if mix-bus mix-bus (rack-in :main)) ; default to main mix bus
        ;; group (if group group chan-g) ; TODO remove dependency on chan-g existing
        bus-in (if (> ins 0) (audio-bus ins (str "IN:" name-str)))
        bus-out (if (> outs 0) (audio-bus outs (str "OUT:" name-str)))
        mix (mix-for-outs bus-out mix-bus group outs)
        ]
    (def rack (assoc rack chan-key {:in bus-in
                                    :out bus-out
                                    :mix mix
                                    :synth synth
                                    }))
    )
  )

;; TODO support non-stereo fx
(defn make-fx-send [from-chan-key to-chan-key]
  (let [
        send-mix (mix-for-outs (rack-out from-chan-key) (rack-in to-chan-key) route-g 2)]
    (def rack (assoc-in rack [from-chan-key :sends to-chan-key] send-mix))
      )
  )

;; TODO: implement for effects, etc
(defmacro play
  "Play a synth in the correct group and output for the rig."
  [gen-key & rest]
  (let [
        gen-name (rack-synth gen-key)
        ]
    `(~gen-name [:tail ~gen-g] ~@rest (~rack-out ~gen-key)))
  )
