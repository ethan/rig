(ns midisynth.osc
  (:require 
            [clojure.core.async
             :refer :all
             :exclude [map reduce into partition partition-by take merge]
             :as async]
            )
  (:use [overtone.osc]))

(def PORT 4241)

                                        ; start a server and create a client to talk with it
(def server (osc-server PORT))
(def client (osc-client "127.0.0.1" PORT))

                                        ; Register a handler function for the /test OSC address
                                        ; The handler takes a message map with the following keys:
                                        ;   [:src-host, :src-port, :path, :type-tag, :args]
(osc-handle server "/test" (fn [msg] (println "MSG: " msg) (swap! counter inc)))

(def counter (atom 0))
                                        ; send it some messages
(doseq [val (range 10)]
  (osc-send client "/test" "a" 2 3 val))

@counter

(Thread/sleep 1000)

                                        ;remove handler
(osc-rm-handler server "/test")

                                        ; stop listening and deallocate resources
(osc-close client)
(osc-close server)

(alter-var-root #'*out* (constantly *out*))
