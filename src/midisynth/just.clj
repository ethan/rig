(ns midisynth.just
  (:use [overtone.live]
        [overtone.synth.sts :only [prophet]])
  (:require
   [overtone.midi :as midi]
   )
  )

(defn intlog2 [n]
  "Get the log2 int value."
  (Math/floor (/ (Math/log n) (Math/log 2)))
  )

(defn dividend2 [n]
  "Determine the divident to offset octave shift from just harmonic mult calc."
  (Math/pow 2 (intlog2 n))
  )


(defn just-ratio [harmonics octave]
  "Determine the ration for a just harmonic set in a given octave."
  (let [harmonic (if (instance? clojure.lang.PersistentVector harmonics)
                    (apply * harmonics)
                    harmonics
                    )
        dividend (dividend2 harmonic)
        octave-mult (Math/pow 2 octave)
        ]
    (* (/ 1 dividend) harmonic octave-mult)
    )
  )

(defn just-cps [root harmonics octave]
  "Determine the pitch of a just note via harmonics."
  (* root (just-ratio harmonics octave))
  )  

(defn just-mode-cps [harmonic-sequence root degree & [octave] ]
  "Return the CPS for a note thats a given order in a given mode"
  (let [octave (if octave octave 0)
        out-of-octave (>= degree (count harmonic-sequence))
        octave (if out-of-octave
                 (+ octave (quot degree (count harmonic-sequence)))
                 octave
                 )
        degree (if out-of-octave
                   (mod degree (count harmonic-sequence))
                   degree
                   )
        ]
    (println harmonic-sequence)
    (just-cps root (nth harmonic-sequence degree) octave)
    )
  )


(defn new-scale-cps [root harmonic-sequence]
  "Generate a function to return note cps for any just mode in a given root"
  (fn [degree & [octave]]
    (just-mode-cps harmonic-sequence root degree octave)
    )
  )

;; Let's set up a map of all the harmonic modes in just.
;; This is taken from Mattieu's Harmonic Experience
(def mode-degrees
  {
   :1 1
   :2 [3 3]
   :b2 [1/5 1/3]
   :3 3
   :b3 [1/3 1/5]
   :4 1/3
   :b4 [3 3]
   :5 3
   :b5 "??"
   :6 [5 1/3]
   :b6 1/5
   :7 [3 5]
   :b7 [3 3 1/5]
   }
  )

;; TODO make a function to pick a vector of mode degrees by index
;; e.g. (scale-from-mode-degrees (:1 :2 :b3 :4 :b5 :6 :b7))

(def mode-harmonics
  {
   :lydian [1 [3 3] 5 [5 3 3] 3 [5 1/3] [5 3]]
   :mixolydian [1 [3 3] 5 1/3 3 [1/3 5] [3 3 1/5]]
   :ionian [1 [3 3] 5 1/3 3 [5 1/3] [3 5]]
   :dorian [1 [3 3] [3 1/5] 1/3 3 [1/3 5] [3 3 1/5]]
   :aeolian [1 [3 3] [3 1/5] [3 3] 3 1/5 [3 3 1/5]]
   :phrygian [1 [1/5 1/3] [1/3 1/5] 1/3 3 1/5 [3 3 1/5]]
   }
  )

;; Trying it out.
(comment
  
  (def scale-cps (new-scale-cps (/ 447 4) (:mixolydian mode-harmonics)))
  (def scale-cps (new-scale-cps (/ 447 4) (:phrygian mode-harmonics)))

  (def scale-cps (new-scale-cps (/ 447 4) (:lydian mode-harmonics)))
  (def scale-cps (new-scale-cps (/ 447 4) (:dorian mode-harmonics)))

  (defn prophet-chord [chord-notes]
    (doseq [note-cps chord-notes]
      (prophet :freq note-cps
               :decay 5 :rq 0.6 :cutoff-freq 2000)
      )
    )

  (prophet-chord [(scale-cps 0 1) (scale-cps 2 1)])
  (prophet-chord [(scale-cps 1 1) (scale-cps 5 0)])
  (prophet-chord [(scale-cps 0 1) (scale-cps 3 1)])
  (prophet-chord [(scale-cps 6 0) (scale-cps 4 1)])


  ;; We use a saw-wave that we defined in the oscillators tutorial
  (comment 
    (definst saw-wave [freq 440 attack 0.01 sustain 0.4 release 0.1 vol 0.4] 
      (* (env-gen (env-lin attack sustain release) 1 1 0 1 FREE)
         (saw freq))
      vol


      (defn saw-chord [chord-notes]
        (doseq [note-cps chord-notes]
          (saw-wave :freq note-cps
                    )
          )
        )))

  (saw-chord [(scale-cps 0 1) (scale-cps 2 1)])
  (saw-chord [(scale-cps 3 1) (scale-cps 5 1)])
  (saw-chord [(scale-cps 1 1) (scale-cps 3 1)])
  (saw-chord [(scale-cps 6 0) (scale-cps 4 1)]))


(comment
  ;; Arp
  ;; Define chords as vectors so we can change the scale-cps function as we wish.
  (def arp-chords [
                   [[6 0] [4 1] [1 2]]
                   [[1 1] [3 1] [5 0]]
                   [[3 1] [5 1] [0 2]]
                   [[0 1] [2 1] [4 1]]
                   ])

  ;; TODO make a cool arpegiator for play chord

  ;; Make a metro.
  (def metro (metronome 128))
  (metro-bpm metro 360)
  (metro-bpm metro 128)
  (metro-bpm metro 80)
  (metro)
  (def arp-notes (nth arp-chords 0))


  ;; TODO add default args
  ;; TODO make a sequencing macro to take care of re-usable bits
  (defn scale-player [beat scale-cps-fn degrees rate]
    (let [degree (* beat rate)
          note-cps (apply scale-cps-fn [(mod degree (- degrees 1))])]
      (at (metro beat)
          (prophet note-cps)
          (apply-by (metro (+ beat (/ 1 rate))) #'scale-player (+ beat (/ 1 rate)) scale-cps-fn degrees rate [])
          )
      )
    )

  (defn scale-player [])

  (scale-cps 5)

  (apply scale-cps [5])

  (scale-player (metro) scale-cps 8 8)

  (stop)

  ;; Every tick pluck the first note off the stack and play it with the function.
  (defn arp-player [beat index]
    (let [arp-notes (nth arp-chords (mod (quot beat 8) (count arp-chords)))]
      (if (> (count arp-notes) 0)
        (at (metro beat)
            (saw-wave (apply scale-cps (nth arp-notes (mod index (count arp-notes))))))
        )
      (apply-by (metro (inc beat)) #'arp-player (inc beat) (mod (inc index) (count arp-notes)) [])
      )
    )

  (defn drone-player [beat]
    (at (metro beat)
        (saw-wave (scale-cps 1 (- (quot (mod beat 8) 2) 2))
                  )
        (saw-wave (scale-cps 1 (- (quot (mod beat 12) 3) 0))
                  0.1 0.4 0.1 0.2)
        )
    (apply-by (metro (+ beat 2)) #'drone-player (+ beat 2) [])
    )

  (defn arp-player [beat index])
  (defn drone-player [beat])




  (defsynth kick [amp 1 decay 0.6 freq 65]
    (let [env (env-gen (perc 0 decay) 1 1 0 1 FREE)
          snd (sin-osc freq (* Math/PI 0.5))]
      (out 0 (pan2 (* snd env amp) 0))))

  (kick)

  (defn kick-player [beat rate amps delays freqs]
    (let [
          index (mod (/ beat rate) (count amps))
          amp (* 1.25 (nth amps index))
          delay (nth delays index)
          freq (nth freqs index)
          ]
      (at (metro beat)
          (kick amp delay freq)
          )
      (apply-by (metro (+ beat rate)) #'kick-player (+ beat rate) rate amps delays freqs[])
      )
    )

  (defn kick-player [beat])

  (drone-player (metro))
  (arp-player (metro) 0)
  (kick-player (metro) 1/5
               [2 0.25 2 2]
               [0.7 0.2 0.6 0.5]
               [(/ 447 8) (/ 447 5) (/ 447 10) (/ 447 5)]
               )

  (stop)


  ;; FM

  (defonce fm-chord-bus2 (audio-bus 2 "fm-chords"))
  (pp-node-tree)
  fm-chord-bus2

  (odoc square)
  (odoc slew)
  (defsynth simple-fm [carrier 440 divisor 2.0 depth 1.0 amp-lfo-speed 1 amp 0.5 out-bus 0]
    (let [modulator (/ carrier divisor)
          amp-lfo (square amp-lfo-speed)
          mod-env   (env-gen (lin 3 0 9))
          amp-env   (env-gen (lin 1 2 10) :action FREE)]
      (out out-bus (pan2 (* amp amp-lfo amp-env
                            (sin-osc (+ carrier
                                        (* mod-env  (* carrier depth) (sin-osc modulator)))))))))

  (defn fm-chords-play [chord-args] 
    (doseq [note-args chord-args]
      (apply simple-fm note-args)
      )
    )

  (defn chord-player [beat rate chord-play-fn chords]
    (let [chords-val (if (var? chords) (var-get chords) chords)
          index (mod (* beat rate) (count chords-val))
          chord (nth chords-val index)
          ]        
      (at (metro beat)
          (apply chord-play-fn (vector chord))
          (apply-by (metro (+ beat (/ 1 rate))) #'chord-player (+ beat (/ 1 rate)) rate chord-play-fn chords [])
          )
      )
    )

  (defn fm-chords-player [beat]

    )

  (def fm-chords
    [
     [
      ;; [(scale-cps 0 0) 3 2 0.5]
      ;; [(scale-cps 0 1) 2 1 1]
      ;; [(scale-cps 5 2) 6 7 1.5]
      ;; [(scale-cps 2 1) 3 3 2]
      ;; [(scale-cps 5 2) 5 2 5]
      ;; [(scale-cps 5 6) (/ 3 7) 9 10 0.25]
      ;; [(scale-cps 5 6) (/ 4 7) 20 9 0.3]
      ;; [(scale-cps 5 6) (/ 5 7) 17 7 0.5]
      ;; [(scale-cps 5 6) (/ 6 7) 22 5 0.7]
      ],
     [
      ;; [(scale-cps 5 -1) (/ 1 5) 12 12]
      ;; [(scale-cps 5 5) (/ 2 7) 8 14]
      ;; [(scale-cps 5 6) (/ 2 7) 8 6]
      ;; [(scale-cps 5 2) 5 2 6]
      ],
     [
      ;; [(scale-cps 3 1) 2 1 2]
      ;; [(scale-cps 0 0) (/ 5 4) 1]
      ;; [(scale-cps 1 0) (/ 3 4) 7 3]
      ;; [(scale-cps 0 1) 2 1 2/4 0.25]
      ;; [(scale-cps 2 1) 1 1 2/5 0.25]
      ;; [(scale-cps 3 1) 1 1 3/5 0.25]
      ;; [(scale-cps 5 1) 1 1 1/6 0.25]
      ;; [(scale-cps 5 -1) (/ 1 5) 12 12]
      ;; [(scale-cps 5 5) (/ 2 7) 8 14]
      ;; [(scale-cps 5 6) (/ 2 7) 8 6]
      ;; [(scale-cps 5 2) 5 2 6]
      ]
     ]
    )

  (chord-player (metro) 1/4 fm-chords-play #'fm-chords)

  (stop)
  (odoc local-out)
  (fm-chords-play [
                   [(scale-cps 0 0) 3]
                   [(scale-cps 0 0) 6]
                   [(scale-cps 0 0) 2]
                   ])

  
  )


