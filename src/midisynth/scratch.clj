(ns midisynth.scratch
  (:use [overtone.live]
        [overtone.synth.sts :only [prophet]]
        [midisynth.sequence]
        )
  (:require
   [midisynth.synths :as synths]
   [rig.rack :as rig]
   [midisynth.fib :as fib]
   [midisynth.webctl :as webctl]
   [midisynth.messages :as messages]
   [midisynth.just :as just]
   [monome-osc.core :as monome]
   [clojure.core.async :as async]
   )
  )

(comment
  ;; Monome playing

  (monome/monitor-devices)
  (def monome (monome/get-device :monome))
  
  (monome/set-all monome 1)  
  (monome/set-all monome 0)  
  
  (def all-events (monome/listen-to monome))
  (def mult-events (async/mult all-events))
  (def test (async/chan))
  (async/tap mult-events test)
  
  (alter-var-root #'*out* (constantly *out*))

  (def log-chan (async/chan))

  (async/thread
    (loop []
      (when-let [v (async/<!! log-chan)]
        (println v)
        (recur)))
    (println "Log Closed"))

  (async/close! log-chan)

  (defonce memory (agent {}))
  
  (defn logit [& msgs]
    (doseq [msg msgs]
      (let [[event values] msg
            ]
        (case event
          :press (let [[x y] values
                       ]
                   (monome/set-led monome x y 1)
                   (send memory
                         (fn [mem]
                           (let [
                                 n (str x y)
                                 fifths (Math/pow 3 (- x 3))
                                 thirds (Math/pow 5 (- y 3))
                                 cps (just/just-cps 440 (* fifths thirds) 0)
                                 s (prophet :freq cps
                                            :decay 5
                                            :rq 0.6
                                            :cutoff-freq 1000
                                            :amp 1)
                                 ]
                             (async/>!! log-chan fifths)
                             (async/>!! log-chan thirds)
                             (assoc mem n s)
                             )
                           )
                         )
                         )
          :release (let [[x y] values]
                   (monome/set-led monome x y 0)
                   (send memory
                         (fn [mem]
                           (let [
                                 n (str x y)
                                 ]
                             (dissoc mem n)
                             )
                           )
                         )
                   )
          )
        
        )
      (async/>!! log-chan (or msg "**nil**"))))
 (recording-start "/Users/ethan/audio/overtone-monome-first-sounds.wav")
 (recording-stop)
 
  (defn log-loop [in]
    (async/go-loop []
      (when-let [e (async/<! in)]
        (logit e)
        (recur)))
    in)
  
  (def logger (log-loop test))
  (async/close! logger)
  )


(comment

  ;; Most of the below (up till the web-ticker) has been implemented in rig.rack-test
  (rig/make-main-chan)
  (rig/make-chan :drone "FM Drone" :synth synths/drone-fm)
  rig/rack

  ;; Playing with web servers within overtone.
  (webctl/start-server {:port 3000})
  (webctl/stop-server)
  
  ;; Signal Synthdefs

  (def fm-drone1 (rig/play :drone (/ 447 4) 2 1 0.5))
  (ctl fm-drone1 :carrier (/ 447 5))
  (ctl fm-drone1 :divisor (/ 1 fib/phi))
  (ctl fm-drone1 :depth 3)
  (def fm-drone2 (rig/play :drone (/ 447 4) 2 1 0.5))
  (ctl fm-drone2 :carrier (+ 2 (/ 447 5)))
  (ctl fm-drone2 :divisor 2/3)
  (ctl fm-drone2 :depth 4)
  (kill fm-drone1)
  (kill fm-drone2)
  
  (def m (metronome 120))
  
  ;; TODO how to refactor this using watches?
  (defn m-web-ticker [metro beat]
    (let [
          ]
      (at (metro beat)
          (messages/put-tick (metro))
          )
      (apply-by (metro (inc beat)) #'m-web-ticker metro (inc beat) [])
      )
    )
  (defn m-web-ticker [])
  
   ;; TODO store the result so we can us at-at/stop
  (m-web-ticker m (m))
  (messages/send-tick)
  
  (messages/put-tick 100)
  
  (metro-bar m)
  (metro-next-periodic-bar m 16)
  (metro-bar m)
  (macroexpand  '(apply-by-next-n-bar m 16
                                      rig/play :drone (/ 447 4) 2 1 0.5 []))
  (stop)
  (odoc saw)
  (odoc impulse)

  (def ss1 (synths/saw-mod-sync-3 110 (* 110 3/2) (* 110 5)))
  (def ss (synths/sync-phasor-3i 110 (* 110 3/2) (* 110 5)))
  (def ss2 (synths/sync-phasor-3i 110 (* 110 3/2) (* 110 5) 1))
  (ctl ss :freq1 (* 110 1))
  (ctl ss :freq2 (* 110 5/4))
  (ctl ss :freq3 (* 110 (* 3 3/5)))
  (ctl ss2 :freq3 (* 110 (* 5 5/3 1/2)))
  ;; Todo experiment with just intonation arpegiation of masters and slaves
  (node-map-controls ss [:freq2 f2-kbus])
  (node-map-controls ss [:freq2 f2-kbus])
  (node-map-controls ss2 [:freq1 f1-kbus])
  (node-map-controls ss2 [:freq2 f2-kbus])
  (kill ss)

  
  (defonce f1-kbus (control-bus))
  (def f1-lfo (tri-osc-k [:before ss] f1-kbus 0.25 40 110))
  (kill f1-lfo)
  
  (defonce f2-kbus (control-bus))
  (def f2-lfo (tri-osc-k [:before ss] f2-kbus 1 110 330))
  (kill f2-lfo)
  
  (defsynth tri-osc-k [out-bus 0 freq 5 mult 1 add 0]
    (out:kr out-bus (+ add (* mult (lf-tri:kr freq)))))

  (defsynth pulse-osc-k [out-bus 0 freq 1 width 0.5 phase 0.0]
    (out:kr out-bus (lf-pulse:kr freq phase width)))

  (stop)
  )

