(ns midisynth.messages
  (:use [clojure.core.async :as async])
  (:require [clojure.data.json :as json]
            [org.httpkit.server :as server]
            )
  )

;; Define a single core async channel called bus to pass internal messages.
(defonce bus (async/chan))
(defonce metro (async/chan 1))
(defonce metro-mult (async/mult metro))

(defn put-tick
  "Log a metro tick"
  [tick-count]
 (async/put! metro {:type "metro" :tick-count tick-count}))

(comment (async/close! metro))

(defn print-tick 
  "Send tick to outputs"
  []
  (async/go-loop []
    (when-let [
               ;; For most cases you tap a mult, not the original channel, unless purging.
               message (async/<! metro)
               ]
      (println message)
      (recur)
      )
    )
  )

(defn register-tick-socket 
  "Send tick to outputs"
  [socket]
  (let [ch (async/chan)]
    (async/tap metro-mult ch)
    (async/go-loop []
      (when-let [
                 ;; For most cases you tap a mult, not the original channel, unless purging.
                 message (async/<! ch)
                 ]
        (server/send! socket (json/write-str message))
        (recur)
        )
      )
    (server/on-close socket (fn [status]
                              (async/untap metro-mult ch)
                              (async/close! ch)
                              ))
    )
  )
