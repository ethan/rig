(ns midisynth.fx-bus
  (:use [overtone.live]
        )
  (:require
   [overtone.midi :as midi]
   [midisynth.rig] ; TODO: alias rig and update to ns symbols
   )
  )


(comment
  ;; Control Synthdefs

  (defsynth tri-osc-k [out-bus 0 freq 5]
    (out:kr out-bus (lf-tri:kr freq)))

  (defsynth pulse-osc-k [out-bus 0 freq 1 width 0.5 phase 0.0]
    (out:kr out-bus (lf-pulse:kr freq phase width)))

  (odoc lf-pulse)

  ;; Signal Synthdefs

  (defsynth drone-fm [carrier 440 divisor 2.0 depth 1.0 amp 0.5 out-bus 0]
    (let [modulator (/ carrier divisor)
          ]
      (out out-bus (pan2 (* amp 
                            (sin-osc (+ carrier
                                        (* (* carrier depth) (sin-osc modulator)))))))))


  ;; Effect Synthdefs

  (defsynth reverb-fx [in-bus 10 out-bus 0 room 0.5 damp 0.5]
    (let [
          input-sig (in in-bus)
          verb-sig (free-verb input-sig room damp)
          stereo-sig (pan2 verb-sig)
          ]
      (out out-bus stereo-sig)))

  ;; Set up control buses

  (defonce amp-kbus (control-bus))
  (defonce frq-kbus (control-bus))
  (defonce div-kbus (control-bus))
  (defonce dpth-kbus (control-bus))

  ;; Set up main routing stuff

  (def chan-main-in (audio-bus 2)) ; the chan we use to mix all sigs to out
  (def main-chan (stereo-chan-router [:tail out-g] chan-main-in 0)) ; map main mix to output 0

  (def chan-fm-drone-out (audio-bus))
  (def fm-drone-chan (mono2stereo-chan-router [:tail chan-g] chan-fm-drone-out chan-main-in))

  (def chan-verb-in (audio-bus))
  (def chan-verb-out (audio-bus 2)) ; verb outs stereo

  (def fm-verb-chan (mono2stereo-chan-router [:tail route-g] chan-fm-drone-out chan-verb-in)) ; route the inst into fx
  (def verb-chan (mono2stereo-chan-router [:tail chan-g] chan-verb-out chan-main-in))

  (ctl verb-chan :level-db 0)
  (ctl fm-drone-chan :level-db 0)
  (ctl main-chan :level-db -0)

  ;; Make Signals

  ;; Set up routing for FM synch

  (def fm-drone1 (drone-fm [:tail gen-g] (/ 447 4) 2 1 0.5 chan-fm-drone-out))

  (ctl fm-drone1 :carrier (/ 447 3))
  (ctl fm-drone1 :divisor 2)
  (ctl fm-drone1 :depth 1)
  (kill fm-drone1)


  (def fm-drone2 (drone-fm [:tail gen-g] (/ 447 3) 2 1 0.5 chan-fm-drone-out))
  (ctl fm-drone2 :carrier (/ 447 5))

  ;; Instantiate Controls

  (def amp-lfo (pulse-osc-k [:tail k-g] amp-kbus))
  (def depth-lfo (tri-osc-k [:tail k-g] dpth-kbus 0.25))
  (def div-lfo (tri-osc-k [:tail k-g] div-kbus 1/8))
  (ctl amp-lfo :freq 8)
  (kill amp-lfo)
  (node-map-controls fm-drone1 [:amp amp-kbus])
  (node-map-controls fm-drone1 [:depth dpth-kbus])
  (node-map-controls fm-drone1 [:divisor div-kbus])

  (node-map-controls fm-drone2 [:amp amp-kbus])
  (node-map-controls fm-drone2 [:divisor dpth-kbus])
  (node-map-controls fm-drone2 [:depth div-kbus])

  ;; Instantiate Effects

  (def verb (reverb-fx [:tail fx-g] chan-verb-in chan-verb-out))
  (ctl verb :room 0.7)
  (ctl verb :damp 0.99)
  (kill verb)


  (stop)



  )

