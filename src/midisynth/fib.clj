(ns midisynth.fib
  (:use [overtone.live]
        [midisynth.just]
        )
  )

;; Expirements using the Fib series and Phi in musical context.


;; from https://en.wikibooks.org/wiki/Clojure_Programming/Examples/Lazy_Fibonacci
(defn fib-lazy-seq []
  ((fn rfib [a b] 
     (cons a (lazy-seq (rfib b (+ a b)))))
   0 1))

(def fib-seq (fib-lazy-seq))

(take 20 fib-seq)

;; Ways of using the fib seq in generative music

;; 1. Additive instrument with overtone amplitudes inverse of the seq

;; 2. Fib seq as a mode

(defn fib-scale-degrees
  "Take the first N elements in the sequence '1 2 3 5 ...'"
  [length]
  (map
   (fn [i]
     (just-ratio i 0)
     )
   (subvec (apply vector (take (+ length 4) fib-seq)) 3))
  )

(comment
  (fib-scale-degrees 8))

(defn fib-ratio-scale-degrees
  "Create a scale based on the ratio of n/n-1 fib numbers."
  [length]
  (map
   (fn [i j]
     (/ j i)
     )
   (subvec (apply vector (take (+ length 2) fib-seq)) 1 (+ length 2))
   (subvec (apply vector (take (+ length 3) fib-seq)) 2 (+ length 3))
   )
  )

(def fib-scale-cps (new-scale-cps 110
                                  (fib-ratio-scale-degrees 8)
                                  ;; (apply vector (sort (fib-ratio-scale-degrees 8)))
                                  ))


(def phi (last (fib-ratio-scale-degrees 50)))

(definst fib-ratio-sin [freq 440 phi-exponent 1]
  "Play a sin wave in unison with a wave n*phi*freq hz."
  (let [phi-mult (pow phi phi-exponent)
        phi-freq (* phi-mult freq)
        ;; main-osc (sin-osc freq)
        ;; phi-osc (sin-osc phi-freq)
        ]
    (+
     (sin-osc freq)
     (sin-osc) phi-freq)
    )
  )

(comment
  (def id (fib-ratio-sin 220 2))
  (kill id)
  )

;; 3. As indexes into another mode

;; 4. As a mode of ratios approaching phi: (n-1/n) (1 1/2 2/3 3/5 5/8 8/13 13/21)
;; TODO make overdrive channel to amplify just resonances.
;; TODO implement remaining modes
