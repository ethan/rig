(ns ethanwinn.laya-bed1
  (:use [overtone.live :refer :all])
  (:require [rig.rack :as rr])
  )

(comment

  (odoc sin)
  (def ons {})
  (defsynth monosin [freq 440 phase 0 amp 0.5 out-bus 0]
    (let [
          sinsig (var-saw freq phase 0.5)
          outsig (* amp sinsig)
          ]
      (out out-bus outsig)
      )
    )

  (defsynth noise1 [amp 0.5 lfofreq 1 lfophase 0 out-bus 0]
    (let [noise (pink-noise)
          lfo (sin-osc lfofreq lfophase)
          ]
      (out out-bus (* lfo noise))
      )
    )

  (def n1 (noise1 0.5 1 0 0))
  (def n2 (noise1 0.5 1 0.5 1))
  
  (defn change-lfo [f]
    (ctl n1 :lfofreq f)
    (ctl n2 :lfofreq f)
    )

  
  (change-lfo 0.125)
  (def test1 (monosin (* 3/2 440) 0 0.5 0))
  (def test2 (monosin (* 5/4 444) 0 0.5 1))
  (def test3 (monosin 440 0 0.5 0))
  (def test4 (monosin 444 0 0.5 1))
  (defn change-range [f]
    (ctl test3 :freq (* f 3/2 220))
    (ctl test4 :freq (* f 5/4 222))
    (ctl test1 :freq (* f 1 220))
    (ctl test2 :freq (* f 1 222))
    )
  (change-range 0.5)
  (change-range 1)
  (change-range 2)
  (change-range 3/2)
  (change-range 5/4)
  (change-range 4/5)
  (def test3 (monosin (* 3/2 110) 0 0.5 ))
  (def test4 (monosin (* 5/4 110) 1 0.5 1))
  
  (map kill [test1 test2 test3 test4])
  (map kill [n1 n2])
)
