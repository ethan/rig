(ns rig.step-sequencer-test
  (:require
   [rig.node :as rn]
   [rig.message :as rm]
   [rig.gear.debug :as r-debug]
   [rig.gear.metro :as r-metro]
   [rig.controllers.step-sequencer :as r-step]
   :reload
   )
  ;; TODO debug why this doesn't work: if I edit step-sequencer and
  ;; redefine some functions, I get method not implemented errors on
  ;; any existing step-sequencer nodes. Why?
  (:import [rn.Node])
  )

(comment
  (def test-debug (r-debug/make-debug-node :test-debug))
  (def test-step (r-step/make-step-sequencer :node-id :test-step
                                              :step-count 16
                                              :tracks [:track-one :track-two]))
  (def x1 (rn/connect :xdebug test-step test-debug (fn [cnct msg] (rn/out-put! cnct msg))))


  (defn make-set-step-message [track step data]
    (rm/make-base-message test-debug :set-track-step {:track track
                                                      :step step
                                                      :step-data data})
    )
  (defn make-play-step-message [step]
    (rm/make-base-message test-debug :play-step {:step step
                                                 })
    )

  ;; Set up the sequence
  (rn/in-put! test-step (make-set-step-message :track-one 0 { :DEBUG 1}))
  (rn/in-put! test-step (make-set-step-message :track-one 3 { :DEBUG 2}))
  (rn/in-put! test-step (make-set-step-message :track-two 7 { :DEBUG 3}))
  (rn/in-put! test-step (make-set-step-message :track-one 13 { :DEBUG 4}))
  (rn/in-put! test-step (make-set-step-message :track-two 13 { :DEBUG 5}))
  (:steps test-step)

  ;; Trigger a single step
  ;; This is currently not working.
  (rn/in-put! test-step (make-play-step-message 13))
  
  (def test-metro (r-metro/make-metro :test-metro 60 4))
  ;; Hook up to the metronome
  (def xfn-metro-step (fn [cnct msg]
                        ;; (println "IN HANDLE")
                        (let [step (mod (-> msg :data :tick) 16)
                              out-msg (make-play-step-message step)]
                                         ;; (println "TO STEP: " out-msg)
                                        (rn/out-put! cnct out-msg)
                                        )
                                      ))

  (def x2 (rn/connect :xstep test-metro test-step xfn-metro-step))
  (dosync (ref-set (:in-handler x2) xfn-metro-step))
  (dosync (ref-set (:in-handler x2) (fn [_ __])))
  ;; Should start to print out sequenced messages.
  (rn/in-put! test-step (rm/make-base-message test-debug :clear-all-steps {}))
  (rn/in-put! test-step (rm/make-base-message test-debug :set-track-steps {:track :track-one :steps-data (map (fn [v] {:trigger v}) [nil nil 1 nil nil nil 1 nil 1 1 nil nil nil nil 1])}))
  (:steps test-step)
  )

