(ns rig.player-test
  (:use
   [overtone.live]
   )
  (:require
   [clojure.core.async
    :as async
    :refer [>! <! >!! <!! go chan close! thread
            alts! alts!! timeout]]
   [rig.rack :as rr]
   [rig.player :as rp]
   [rig.node :as rn]
   [rig.message :as rm]
   [org.httpkit.server :as h]
   [ring.middleware.reload :as ring-reload]
   [clojure.data.json :as json]
   [clojure.walk :as walk]
    ;; :reload ; Uncomment for easier testing.
   )
  )

;; TODO implement real tests.
;; For now wrap a bunch of ad hoc tests in a comment block.
(comment
  ;; Print output from threads to repo.
  (alter-var-root #'*out* (constantly *out*))
  
  ;; Set up the rig
  (defsynth pad2 [freq 440 amp 0.4 amt 0.3 gate 1.0 out-bus 0]
    (let [vel        (+ 0.5 (* 0.5 amp))
          env        (env-gen (adsr 0.01 0.1 0.7 0.5) gate 1 0 1 FREE)
          f-env      (env-gen (perc 1 3))
          src        (saw [freq (* freq 1.01)])
          signal     (rlpf (* 0.3 src)
                           (+ (* 0.6 freq) (* f-env 2 freq)) 0.2)
          k          (/ (* 2 amt) (- 1 amt))
          distort    (/ (* (+ 1 k) signal) (+ 1 (* k (abs signal))))
          gate       (pulse (* 2 (+ 1 (sin-osc:kr 0.05))))
          compressor (compander distort gate 0.01 1 0.5 0.01 0.01)
          dampener   (+ 1 (* 0.5 (sin-osc:kr 0.5)))
          reverb     (free-verb compressor 0.5 0.5 dampener)
          echo       (comb-n reverb 0.4 0.3 0.5)]
      (out out-bus
           (* vel env echo))))
  (rr/make-main-chan)
  (rr/make-chan :pad "Pad" :synth pad2)
  rr/rack ; should have a ton of stuff

  ;; Set up the rig player for a synth
  
  (def pad-player  (rp/make-player
                    :node-id :player-pad2
                    ;; We specity the xform-fn function separate in any case, whether
                    ;; using idiomatic rig setup or custom do-* funcs:
                    :args-xform-fn (fn
                                     [message]
                                     (let [{:keys [midi-note amp]} (rm/get-data message)
                                           freq (midi->hz midi-note)
                                           ]
                                       ;; TODO test if "nil" for amp causes a 0 amp value
                                       ;; or uses the definst default.
                                       (println message)
                                       [freq amp]
                                       )
                                     )
                    :synth-key :pad
                    ))

  (def pad-test (rr/play :pad 440))
  (ctl pad-test :gate 0)
  
  ;; Put some messages in its queue, it should make sound!
  (def pad-message (rm/make-base-message :test :synth-on {:midi-note 60 :amp 0.6 :id 60}))
  (def pad-off-message (rm/make-base-message :test :synth-off {:id 60}))
  pad-message
  (rm/get-event pad-message)
  ;; This is one weird thing about extending types and ns's: the methods stay in the protocols ns.
  ;; TODO refactor this to be a "send-in" method on node that handles basic sending of a single (or multiple?) message.
  (rn/in-chan-put! pad-player pad-message)
  (rn/in-chan-put! pad-player pad-off-message)
  (:instances pad-player)  
  (rn/rm-node pad-player)
  

  
  ;; Additional tests for webctl

  ;; TODO: move these to webctl_test
  (defn app
    "Lame app for quick hacking."
    [request]
    (h/with-channel request socket
      (h/on-receive socket (fn [message]
                                  (let [msg (-> message
                                                (json/read-str)
                                                (walk/keywordize-keys)
                                                )
                                        note-status (get-in msg [:data :value])]
                                    (println note-status)
                                    ;; TODO: why is the status always "on"?
                                    (if (= note-status "on")
                                      (rn/in-chan-put! pad-player pad-message)
                                      (rn/in-chan-put! pad-player pad-off-message)
                                      )
                                    )
                                  ))
      )
    )

  (defonce server (atom nil))

  (comment
    (start-server {:port 3000})
    )

  (defn start-server [options]
    (reset! server (h/run-server (ring-reload/wrap-reload #'app) options)))

  (defn stop-server []
    (when-not (nil? @server)
      (@server :timeout 100)
      (reset! server nil)))

  (defn -main [& args]
    (start-server {:port 3000})
    (println "Ready."))

  (comment
    (stop-server)
    )
  
)
