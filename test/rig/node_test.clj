(ns rig.node-test
  (:require
   [clojure.core.async
    :as async
    :refer [>! <! >!! <!! go chan buffer close! thread
            alts! alts!! timeout]]
   [rig.node :as rn]
   [rig.message :as rm]

   :reload-all ; Uncomment for easier testing.
   )
  )

;; TODO implement real tests.
;; For now wrap a bunch of ad hoc tests in a comment block.
(comment
  (def counter1 0)
  (def n1 (rn/make-node (fn [node message] (def counter1 (+ counter1 (:amt (rm/get-data message)))))))
  (rn/get-in-chan n1)
  (def m1 (rm/make-base-message :repl :debug-event {:amt 10}))
  (rn/in-chan-put! n1 m1)
  counter1
  ;; should be 10

  ;; TODO determine how we can change the in-handler-fn "property" on an existing instance of a node.
  ;; (currently assoc creates a new instance but the existing in! loop uses the old in-handler-fn and assoc! fails)
  (assoc! n1 :in-handler-fn (fn [node message]
                             (def counter1 (+ counter1 20 (:amt (rm/get-data message))))
                             ))
  )
