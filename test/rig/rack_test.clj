(ns rig.rack-test
  (:use
   [overtone.live])
  (:require
   [clojure.core.async
    :as async
    :refer [>! <! >!! <!! go chan close! thread
            alts! alts!! timeout]
    ]
   [rig.rack :as rr]
   ;; :reload-all ; Uncomment for easier testing.
   )
  )

;; TODO implement real tests.
;; For now wrap a bunch of ad hoc tests in a comment block.
(comment

  (defsynth drone-fm [carrier 440 divisor 2.0 depth 1.0 amp 0.5 out-bus 0]
    (let [modulator (/ carrier divisor)
          ]
      (out out-bus (pan2 (* amp 
                            (sin-osc (+ carrier
                                        (* (* carrier depth) (sin-osc modulator)))))))))

  (rr/make-main-chan)
  (rr/make-chan :drone "FM Drone" :synth drone-fm)
  rr/rack ; should have a ton of stuff
  
  (def fm-drone1 (rr/play :drone (/ 447 4) 2 1 0.5))
  ;; That should make a sound.
  (kill fm-drone1)
  
  )
