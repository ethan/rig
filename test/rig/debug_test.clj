(ns rig.debug-test
  (:require
   [rig.gear.debug :as rdb]
   [rig.node :as rn]
   [rig.message :as rm]
   :reload
   )
  )

(comment
  (def test-debug (rdb/make-debug-node :midi-test-debugger))
  (def test-message (rm/make-base-message test-debug :test-message {:data nil}))
  (rn/in-put! test-debug test-message)
  (dosync (ref-set (:in-handler test-debug) (fn [node message] { (println node) (println message)})))
  ;; -> should output the message content in the REPL.
  (rn/rm-node test-debug)
  )
