# Introduction to Rig

Rig allows you to create "patchable" assemblages of SuperCollider
synths, physical interfaces, virtual controllers and anything else you
can pipe through Clojure. It started as an exploration in applying an
FRP-like dataflow-based approach to working with Overtone. By using
messages passed over Clojure async channels Rig allows for the
creation of composable graphs of synth players and controllers that
can route, transform, record, etc. all control signals passing through
the system. This makes it easy to create evolving loops modeled on a
brief sequence of button presses on your monome, or incoporating
ClojureScript-based web interfaces into Overtone works.

Rig, itself, is made up of a few main components:

* `rig.rack` is a (fairly opinionated) SuperCollider group
  configuration and related helper routines implemented via Overtone's
  API. It abstracts creating synths, effects, control channels,
  etc. and makes them easy to work with. It's modeled on a standard
  signal flow starting with control signals, then generators, effects
  and finally mixdown.
* `rig.node` and `rig.message` implement the basic graph: creating
  nodes that have inputs, outputs and event loops for handling
  messages and the default message protocol those graph nodes
  understand. `rig.node` also includes a `connection` method for
  connecting two nodes, with an optional message transform function in
  the middle.
* `rig.controllers.*` contains myriad nodes that are used to produce
  control signals for the graph. This might be a physical controller
  like a Monome or MIDI keyboard, an internal controller like a step
  sequencer or a virtual controller like a web interface.
* `rig.gear.*` contains miscellaneous utility nodes like metronomes
  (which should probably be moved to controllers), common utility
  preprocessing nodes (such as rig.monome.region *TODO*), etc.
* `rig.synths.*` includes some common synths (*TODO*: and their
  respective rig player node factory functions).

## RIG IS A WORK IN PROGRESS

Rig is very much in-development at present. It's still mostly the playground for Ethan Winn to explore Clojure's data-based approach in the context of making music and will likely have radical breaking changes.

## Major anticipated updates:

* Create multiple `*.core` namespaces (`rig.core`, `rig.gear.core`, etc.)
* Move all node types into `rig.gear` or otherwise standardize organization.
* Add player functions for common synths.
* Implement a bunch of planned synths (see below).

## Rig Gear

### Sequencers

* `rig.gear.step-sequencer` basic step sequencer for sending data packets
  organized by one or more channel.
* TODO `rig.gear.stream_sequencer` record and playback messages based on
  timestamp and offset from start.

### Monome

* `rig.controllers.monome` implements basic translation of monome OSC
  messages to rig message streams.
  * TODO move to rig.gear.monome.
* TODO `rig.controllers.monome region` implements a tranformation node
  that maps "root-position" monome signals to a region, limiting width
  and height of the region that can be interacted with by a given
  stream and applying the needed offsets. Useful for composing
  multiple monome interfaces that don't require the full dimensions.
* TODO `rig.gear.monome.just-degrees` map one or more rows of a
  monome to play the scale degrees of a justly intoned mode. Each row
  corresponds to a different octave.
  * TODO Offer "keydown", "latch" or "toggle" modes. Keydown plays on
    button down, stops on button up. Latch sustains groups, where
    group is defined as long as one or more buttons is held down
    continuously. Toggle starts play on first button down and stops
    play on next button down.
* TODO `rig.gear.monome.just-lattice` map one or more 4x4 monome
  regions to play a synth via the harmonic lattice (Y maps to thirds
  and X to fifths, with tonic at the `[1 1]` position and one
  reciprocal third and fifth degree).
* TODO `rig.gear.monome.ctl-grid` generate 2 dimensional control
  signals based on coordinates of button pushed. For example control
  "ratio" and "modulation" parameters for an FM synth via steps.
* TODO `rig.gear.monome.step-sequencer` control
  rig.gear.step-sequencer via monome.
* TODO `rig.gear.monome.switch` switch between monome layout.
  * TODO implement "dump" standard for monome gear so monome will
    update to current state when switching between layouts / functions.

### MIDI Device

* `rig.gear.midi` handle basic midi device (key up/down and control messages).
* `rig.gear.midi.keyboard` control a rig player via a rig.gear.midi
  keyboard. Offers keydown, latch and toggle modes.

### Synths

* FM
* Additive
* Sample
* Looper
* Granular
