# Rig

Rig is all about patching pieces together to make music. Using channels and
transducers to manage all message playing and eventing code for Overtone live
coding, Rig is intended to make it easy to patch together MIDI controllers,
custom sequences, OSC sources and even web interfaces to interface with
Overtone and control SuperCollider synths.

## Usage

See [doc/intro.md](doc/intro.md) for in-depth docs.

## License

Copyright © 2015 Ethan Winn

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
